$(document).ready(function () {
    let isLowRes = window.matchMedia("only screen and (max-width: 999px)").matches;

    $('.speech-txt').scrollbar();

    $('.burger').click(function(e){
        e.preventDefault();
        (this.classList.contains("active") === true) ? this.classList.remove("active") : this.classList.add("active");

        $('.head-nav').toggleClass('active');
        $('body').on('click', function (e) {
            var div = $('.head-nav, .burger');

            if (!div.is(e.target) && div.has(e.target).length === 0) {
                div.removeClass('active');
            }
        });
    });


    /* Init sliders */
    $('.main-slider').slick({
        dots: false,
        infinite: true,
        speed: 500,
        arrows: true,
        autoplay: true,
        autoplaySpeed: 9000,
        swipeToSlide: true,
        speed: 200
    });

    $('.main-slider').on('afterChange', function(event, slick, direction){
        $('.nav-slides .active').removeClass('active');
        $('.nav-slides .item[data-slide-num="' + direction + '"]').addClass('active');
    });
    /* Init sliders end */ 

    /* Slider controls */
    $('.nav-slides .item').click(function(e){
        let elem = $(this);

        $('.nav-slides .active').removeClass('active');
        
        elem.addClass('active');

        $('.main-slider').slick('slickGoTo', (elem.data('slide-num')));
    });
    /* Slider controls END */

    /* Lazy youtube */
    var youtube = document.querySelectorAll(".youtube");
	
	for (var i = 0; i < youtube.length; i++){
		var source = "https://img.youtube.com/vi/" + youtube[i].dataset.embed + "/maxresdefault.jpg";
        
		var image = new Image();
		image.src = source;
		image.addEventListener("load", function(){
			youtube[ i ].appendChild(image);
		}( i ) );

		youtube[i].addEventListener("click", function(){
			var iframe = document.createElement("iframe");
			iframe.setAttribute("frameborder", "0");
			iframe.setAttribute("allowfullscreen", "");
			iframe.setAttribute("src", "https://www.youtube.com/embed/"+ this.dataset.embed +"?rel=0&showinfo=0&autoplay=1");
            iframe.setAttribute("allow", "autoplay");

			this.innerHTML = "";
			this.appendChild(iframe);
		} );	
    };
    /* Lazy youtube END */

    $('.scroll-to').click(function(e){
        e.preventDefault();
        let target = $(this).data('target'),
            padding;

        if (isLowRes){
            padding = 52;
        } else{
            padding = 60;
        }
        console.log(padding);

        $('.head-nav, .burger').removeClass('active');

        $('html, body').animate({
            scrollTop: $(target).offset().top - padding
        }, 500);
    })
});